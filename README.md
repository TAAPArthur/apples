# apples
A caffine repacement; daemon that disables the screensaver when an application is full screen

## Build & Install
```
make
make install
```

## Example
```
xset $LEN $PERIOD
apples $SCREENSAVER_CMD $CYCLE_CMD
```
After $LEN seconds of inactivity, $SCREENSAVER_CMD will be run. After an
additional $PERIOD seconds, $CYCLE_CMD will be run and will continue to ever
$PEROID seconds until there's activity.
